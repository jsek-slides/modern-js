class ProgressExtension {

    constructor () {
        this.options = {
            position: `top`,
            height: `5px`,
            color: 'cyan',
            delay: 1000,  // prevent printing to PDF
        };
    }

    init() {
        this.progressDiv = document.createElement("div");
        this.progressDiv.className = 'progress';
        document.body.appendChild(this.progressDiv);

        this.render(false);

        setTimeout(() => {
            this.render(true);
        }, this.options.delay);

        big.onNext(_ => {
            this.render(true);
        });

        big.onModeChange(mode => {
            if (/(jump|print)/.test(mode)) {
                this.progressDiv.style = 'display: none';
            }
            if (/talk/.test(mode)) {
                this.progressDiv.style = 'display: block';
            }
        });

        return Promise.resolve();
    }

    render(ready) {
        let progress = 100 * big.current / (big.length - 1);
        this.progressDiv.style = `
            position: fixed;
            height: ${this.options.height};
            width: ${progress}%;
            background: ${this.options.color};
            ${this.options.position}: ${ready ? '0' : '-' + this.options.height};
            transition: top ease 300ms;
            left: 0;
        `;
    }
}

window.progress = new ProgressExtension();