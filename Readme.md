# Top 5 improvements in modern JavaScript

## Setup

- Run `yarn`

### Run on localhost

```shell
yarn dev
```

### Agenda

1. Why would you care? (Market, StackOverflow / Docs, Courses)
1. Why not TypeScript?
1. Fetch instead of `$.ajax()`
2. `includes()` (do we have polyfill in `Utils.ts`?)
3. Object.keys / assign / values / entries
4. `...` Rest & Spread
  - `$ = (s) => [...document.querySelectorAll(s)]`
5. Async/Await
6. Setting up Webpack + Babel

### Exported PDF version

- See [pdf](?)

### Preview

<!-- ![](images/screenshot.png) -->

# Smalltalk - Top 5 improvements in modern JavaScript
(nice idea before diving into Service Workers or Vue.js)

Goal: Make them write better JavaScript code (teaching)

Outline
- Why would you care? (Market, StackOverflow / Docs, Courses)
- Why not TypeScript?

---

What we will skip:

- Variable declaration: ~~`var`~~, `const`, `let`
- Template literals: `${}`
- Trailing `,` are allowed
- Arrow functions: `=>`
- `class`, `static`
- Set / Map / Symbol
- [Proxy](https://codeburst.io/understanding-javascript-proxies-by-examining-on-change-library-f252eddf76c2)

---

What we will cover:

1. Fetch
2. `includes()` (do we have polyfill in `Utils.ts`?)
3. Object.keys/assign/values/entries
4. `...` Rest & Spread
  - `$ = (s) => [...document.querySelectorAll(s)]`
5. Async/Await

---

Beyond ES8 -> Observables | Use Rx.js (show autocomplete implementation)

---

- http://kangax.github.io/compat-table/es2016plus/

Resources
- [Cheatsheet](https://github.com/mbeaudru/modern-js-cheatsheet)
- [What’s new in ES2018?](https://slidr.io/mathiasbynens/what-s-new-in-es2018)
by Mathias Bynens
- [for-await-of and synchronous iterables](http://2ality.com/2017/12/for-await-of-sync-iterables.html)
- [Object rest and spread properties](https://developers.google.com/web/updates/2017/06/object-rest-spread)

Full lists of changes:
- http://exploringjs.com/es6/index.html
- http://exploringjs.com/es2016-es2017/index.html
- http://exploringjs.com/es2018-es2019/toc.html

```js
[...document.querySelectorAll('ul ul ul ul')]
.forEach(ul => ul.remove())
```



Goal: Avoid blocking UI interactions

Before:
```js
function asyncSum(a, onFinished) {
  setTimeout(function() {
    if (a.length < 2) {
      onFinished(a[0]);
    }
    else {
      asyncSum(a.slice(1), function(x) {
        onFinished(x + a[0]);
      });
    }
  }, 10);
}
```

After:
```js
async function asyncSum(a) {
  let sum = 0;
  for await (const x of a){
    sum += x;
  }
  return sum;
}
```

### RegExp Named Group Captures

`?<name>`

Before:
```js
var match = /([0-9]{4})-([0-9]{2})-([0-9]{2})/u.exec('2015-02-01');
console.log({ year: match[1], month: match[2], day: match[3] });
```

After:
```js
/(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/u
.exec('2015-02-01')
.groups
```

```js
//ECMAScript 2018
/first.second/s.test('first\nsecond'); //true   Notice: /s
```

4.2 Using Named groups inside regex itself
We can use the \k<group name> format to back reference the group within the regex itself. The following example shows how it works.

```js
/(?<fruit>apple|orange)==\k<fruit>/u.test('apple==apple')
```

![](https://cdn-images-1.medium.com/max/800/1*weBLy9CAXFnWNwUqcwNMAg.png)

![](https://cdn-images-1.medium.com/max/800/1*MJuEF0dePb_NE8DHFm0XNw.png)