# Top 5 improvements in JavaScript

since ES5

---

# ![bg:Logo](https://www.bram.us/wordpress/wp-content/uploads/2016/06/javascript-logo-banner.jpg)

<!-- https://cdnhtml5hive.azureedge.net/wp-content/uploads/2014/06/js_800x800-619x619.jpg?x25428 -->

---


- ① Why worry?
- ② Code, code, ...
- ③ Webpack + Babel

---

<p class="q"><span class="circle">1</span></p>

---

### Future is here

- StackOverflow answers
- New-hires are learning ES7+
- Updated docs for libraries
- TypeScript / Babel.js
- Node.js

---

```grid(1:1,#151515)
It will make us more productive...
```

---

```grid(1:1,brown)
...although CoffeeScript had it all along
```

---

TypeScript?

---

***NOISE***

---

***Some breaking changes will piss you off just like C++ compiler updates***

---

Where do you expect it to appear?

---

Only in _Angular_ documentation

---

<p class="q"><span class="circle">2</span></p>

---

### What we will skip:

- Variable declaration: ~~`var`~~, `const`, `let`
- Template literals: `${}`
- Trailing `,` are allowed
- Arrow functions: `=>`
- `class`, `static`
- Set / Map / Symbol
- ...

---

What we will cover:

1. Fetch
2. `includes()` (do we have polyfill in `Utils.ts`?)
3. Object.keys/assign/values/entries
4. `...` Rest & Spread
  - `$ = (s) => [...document.querySelectorAll(s)]`
5. Async/Await

---

```grid(3:2,#0B4C70:#0D635F,gap:.3em)
Fetch  | Rest  | Spread
Object | Regex | Async/Await
```

---

```js
function asyncSum(a, onFinished) {
  setTimeout(function() {
    if (a.length < 2) {
      onFinished(a[0]);
    }
    else {
      asyncSum(a.slice(1), function(x) {
        onFinished(x + a[0]);
      });
    }
  }, 10);
}
```

```js
async function asyncSum(a) {
  let sum = 0;
  for await (const x of a){
    sum += x;
  }
  return sum;
}
```

---

Before:
```js
var match = /([0-9]{4})-([0-9]{2})-([0-9]{2})/u.exec('2015-02-01');
var date = { year: match[1], month: match[2], day: match[3] };

// { year: "2015", month: "02", day: "01" }
```

After:
```js
var date = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/u
    .exec('2015-02-01')
    .groups;

// { year: "2015", month: "02", day: "01" }
```

---

<p class="q"><span class="circle">3</span></p>

---

```shell
> npm install -D webpack babel
```

---

```js
async function open (browser, url) {
    const page = await browser.newPage();
    await page.setViewport({width: 1024, height: 640});
    await page.waitForFunction('window.innerWidth > 1000');
    await page.emulateMedia('screen');
    await page.goto(url);
    return page;
}
```

---

### Summary 
 
- ?

---

##### Thanks :-)

---

### Resources

- [Cheatsheet](https://github.com/mbeaudru/modern-js-cheatsheet)
- [What’s new in ES2018?](https://slidr.io/mathiasbynens/what-s-new-in-es2018)
by Mathias Bynens
- [for-await-of and synchronous iterables](http://2ality.com/2017/12/for-await-of-sync-iterables.html)
- [Object rest and spread properties](https://developers.google.com/web/updates/2017/06/object-rest-spread)

